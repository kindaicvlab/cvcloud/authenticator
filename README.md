[![Pipeline Status](https://gitlab.com/kindaicvlab/cvcloud/authenticator/badges/master/pipeline.svg)](https://gitlab.com/kindaicvlab/cvcloud/authenticator/-/pipelines)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/kindaicvlab/cvcloud/authenticator)](https://goreportcard.com/report/gitlab.com/kindaicvlab/cvcloud/authenticator)
[![Coverage](https://gitlab.com/kindaicvlab/cvcloud/authenticator/badges/master/coverage.svg)](https://gitlab.com/kindaicvlab/cvcloud/authenticator/-/commits/master)

# authenticator

Generate and Delivery kubeconfig for ServiceAccounts.

## Deploy

```shell
$ kustomize build manifests/overlays/release | kubectl apply -f -
```

### Options

#### authenticator create

```shell
$ authenticator create --help
create kubeconfig for ServiceAccount

Usage:
  authenticator create [flags]

Flags:
  -e, --api-server string     endpoint for kube-apiserver (default "127.0.0.1:6443")
  -c, --cluster-name string   cluster name for kubeconfig (default "kubernetes")
  -h, --help                  help for create
  -l, --selector string       selector for ServiceAccount (eg. project=alpha)
```

#### config-reloader reload

```shell
$ reloader reload --help
reload authenticator server

Usage:
  config-reloader reload [flags]

Flags:
  -h, --help               help for reload
  -n, --namespace string   namespace in which authenticator is installed (default "authenticator")
  -l, --selector string    selector for authenticator Deployment (eg. app.kubernetes.io/component=authenticator) (default "app.kubernetes.io/name=authenticator")
```

### Use Ingress (optional)

If you want to use to publish `authenticator`, add the following manifest for Ingress.   

```yaml
# Sample for Ingress Nginx Controller
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: authenticator-ingress
  namespace: authenticator
  annotations:
    nginx.ingress.kubernetes.io/use-regex: "true"
spec:
  ingressClassName: foo.bar
  rules:
      # you need to create ingressClass named `fiz.buz`.
    - host: "fiz.buz"
      http:
        paths:
          - pathType: ImplementationSpecific
            path: "/kubeconfig/.*"
            backend:
              service:
                name: authenticator
                port:
                  name: http
```
