/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kubectl

import (
	"context"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/labels"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// GetServiceAccount gets serviceAccounts managed by cks
func GetServiceAccount(ctx context.Context, cli client.Client, selector map[string]string) (*corev1.ServiceAccountList, error) {
	serviceAccounts := &corev1.ServiceAccountList{}
	if err := cli.List(ctx, serviceAccounts, &client.ListOptions{
		LabelSelector: labels.SelectorFromSet(selector),
	}); err != nil {
		return serviceAccounts, err
	}
	return serviceAccounts, nil
}

// GetSecret gets secret matching with secret's name and namespace
func GetSecret(ctx context.Context, cli client.Client, secretName, namespace string) (*corev1.Secret, error) {
	secret := &corev1.Secret{}
	if err := cli.Get(ctx, client.ObjectKey{
		Name:      secretName,
		Namespace: namespace,
	}, secret); err != nil {
		return secret, err
	}
	return secret, nil
}

// GetNamespace gets namespace
func GetNamespace(ctx context.Context, cli client.Client, nsName string) (*corev1.Namespace, error) {
	ns := &corev1.Namespace{}
	if err := cli.Get(ctx, client.ObjectKey{Name: nsName}, ns); err != nil {
		return nil, err
	}
	return ns, nil
}
