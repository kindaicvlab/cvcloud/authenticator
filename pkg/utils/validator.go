/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package utils

import (
	"context"
	"fmt"
	"strings"

	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/kindaicvlab/cvcloud/authenticator/pkg/kubectl"
)

func ValidateSelector(selector string) (map[string]string, error) {
	if selector == "" {
		return make(map[string]string), nil
	}
	splitSelector := strings.Split(selector, "=")
	if len(splitSelector) != 2 {
		return nil, fmt.Errorf("failed to parse selector option, %s; must be in the format of 'key=value'", selector)
	}
	return map[string]string{splitSelector[0]: splitSelector[1]}, nil
}

func ValidateNamespace(ctx context.Context, nsName string, c client.Client) bool {
	_, err := kubectl.GetNamespace(ctx, c, nsName)
	return err == nil
}
