/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package reload

import (
	"context"
	"fmt"
	"testing"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/utils/pointer"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/kindaicvlab/cvcloud/authenticator/pkg/config-reloader/consts"
	"gitlab.com/kindaicvlab/cvcloud/authenticator/pkg/utils"
)

func TestReloadCommand(t *testing.T) {
	testCases := []struct {
		description    string
		deleteObjKinds []string
		wantError      bool
	}{
		{
			description: "Positive Case",
			wantError:   false,
		},
		{
			description:    "Failed to find namespace",
			wantError:      true,
			deleteObjKinds: []string{"Namespace"},
		},
		{
			description:    "Failed to find Deployment",
			wantError:      true,
			deleteObjKinds: []string{"Deployment"},
		},
	}

	for _, test := range testCases {
		t.Run(test.description, func(t *testing.T) {
			ctx := context.Background()
			cli := &kubeClientMock{}
			c, err := cli.GetClient()
			if err != nil {
				t.Fatal(err)
			}
			if test.deleteObjKinds != nil {
				if err := deleteTestObj(ctx, test.deleteObjKinds, c); err != nil {
					t.Fatal(err)
				}
			}

			o := &reloadOptions{
				authenticatorNamespace: consts.DefaultAuthenticatorNamespace,
				authenticatorSelector:  consts.DefaultAuthenticatorServerSelector,
			}
			err = o.run(ctx, c)
			if (err != nil) != test.wantError {
				t.Errorf("expected error: %v, got: %v\n", test.wantError, err)
			} else if !test.wantError {
				deploy, err := newFakeAuthenticatorDeploy()
				if err != nil {
					t.Fatal(err)
				}
				getDeploy := &appsv1.Deployment{}
				if err = c.Get(ctx, client.ObjectKeyFromObject(deploy), getDeploy); err != nil {
					t.Fatal(err)
				}
				deployAnnotation := getDeploy.Spec.Template.ObjectMeta.Annotations
				if deployAnnotation == nil {
					t.Errorf("Authenticator Deployment does not have annotation in 'spec.template.metadata'")
				}
				if _, ok := deployAnnotation[consts.RestartDateAnnotationKey]; !ok {
					t.Errorf("Authenticator Deployment does not have annotation, <%s> in 'spec.template.metadata'",
						consts.RestartDateAnnotationKey)
				}
			}
		})
	}
}

func deleteTestObj(ctx context.Context, deleteObjKind []string, c client.Client) error {
	for _, objKind := range deleteObjKind {
		var (
			obj    client.Object
			objKey client.ObjectKey
		)
		switch objKind {
		case "Namespace":
			obj = &corev1.Namespace{}
			objKey = client.ObjectKeyFromObject(newFakeNamespace())
		case "Deployment":
			obj = &appsv1.Deployment{}
			deploy, err := newFakeAuthenticatorDeploy()
			if err != nil {
				return err
			}
			objKey = client.ObjectKeyFromObject(deploy)
		default:
			return fmt.Errorf("you must specify 'Namespace' or 'Deployment'")
		}

		if err := c.Get(ctx, objKey, obj); err != nil {
			return err
		}
		if err := c.Delete(ctx, obj, &client.DeleteOptions{}); err != nil {
			return err
		}
	}
	return nil
}

type kubeClientMock struct{}

func (c *kubeClientMock) GetClient() (client.Client, error) {
	scm := runtime.NewScheme()
	if err := scheme.AddToScheme(scm); err != nil {
		return nil, err
	}
	fakeClientBuilder := fake.NewClientBuilder().WithScheme(scm)

	deploy, err := newFakeAuthenticatorDeploy()
	if err != nil {
		return nil, err
	}
	ns := newFakeNamespace()
	fakeClientBuilder.
		WithObjects(ns).
		WithObjects(deploy)
	return fakeClientBuilder.Build(), nil
}

func newFakeAuthenticatorDeploy() (*appsv1.Deployment, error) {
	authSelector, err := utils.ValidateSelector(consts.DefaultAuthenticatorServerSelector)
	if err != nil {
		return nil, err
	}
	return &appsv1.Deployment{
		TypeMeta: metav1.TypeMeta{
			APIVersion: appsv1.SchemeGroupVersion.String(),
			Kind:       "Deployment",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "test-authenticator",
			Namespace: consts.DefaultAuthenticatorNamespace,
			Labels:    authSelector,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: pointer.Int32(1),
			Selector: &metav1.LabelSelector{
				MatchLabels: authSelector,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: authSelector,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "auth",
							Image: "alpine:3.15",
						},
					},
				},
			},
		},
	}, nil
}

func newFakeNamespace() *corev1.Namespace {
	return &corev1.Namespace{
		TypeMeta: metav1.TypeMeta{
			APIVersion: corev1.SchemeGroupVersion.String(),
			Kind:       "Namespace",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: consts.DefaultAuthenticatorNamespace,
		},
	}
}
