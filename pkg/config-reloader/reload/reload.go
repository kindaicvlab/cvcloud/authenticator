/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package reload

import (
	"context"
	"fmt"
	"time"

	"github.com/spf13/cobra"
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/klog/v2"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/kindaicvlab/cvcloud/authenticator/pkg/config-reloader/consts"
	"gitlab.com/kindaicvlab/cvcloud/authenticator/pkg/kubectl"
	"gitlab.com/kindaicvlab/cvcloud/authenticator/pkg/utils"
)

type KubeClient interface {
	GetClient() (client.Client, error)
}

type reloadOptions struct {
	authenticatorSelector  string
	authenticatorNamespace string
}

// NewReloadCmd is command to reload authenticator server
func NewReloadCmd() *cobra.Command {

	o := &reloadOptions{}
	command := &cobra.Command{
		Use:          "reload",
		Short:        "reload authenticator server",
		Long:         `reload authenticator server`,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			cli := &kubectl.KubeClient{}
			c, err := cli.GetClient()
			if err != nil {
				return err
			}
			return o.run(context.Background(), c)
		},
	}
	f := command.Flags()
	f.StringVarP(&o.authenticatorSelector, "selector", "l", consts.DefaultAuthenticatorServerSelector,
		"selector for authenticator Deployment (eg. app.kubernetes.io/component=authenticator)")
	f.StringVarP(&o.authenticatorNamespace, "namespace", "n", consts.DefaultAuthenticatorNamespace,
		"namespace in which authenticator is installed")
	return command
}

func (o *reloadOptions) run(ctx context.Context, c client.Client) error {
	splitSelector, err := utils.ValidateSelector(o.authenticatorSelector)
	if err != nil {
		klog.Errorf("Failed to verify selector, <%s>.", o.authenticatorSelector)
		return err
	}
	if !utils.ValidateNamespace(ctx, o.authenticatorNamespace, c) {
		klog.Errorf("Failed to find namespace, <%s>", o.authenticatorNamespace)
		return fmt.Errorf("failed to find namespace, <%s>", o.authenticatorNamespace)
	}

	deploy := &appsv1.DeploymentList{}
	if err := c.List(ctx, deploy, &client.ListOptions{
		LabelSelector: labels.SelectorFromSet(splitSelector),
	}); err != nil {
		klog.Errorf("Failed to list authenticator server Pod.")
		return err
	}

	if len(deploy.Items) == 0 {
		klog.Warning("There are not any authenticator server in cluster.")
		return fmt.Errorf("there are not any authenticator server in cluster")
	}

	for _, dep := range deploy.Items {
		newDep := dep.DeepCopy()
		if newDep.Spec.Template.ObjectMeta.Annotations == nil {
			newDep.Spec.Template.ObjectMeta.Annotations = make(map[string]string)
		}
		newDep.Spec.Template.ObjectMeta.Annotations[consts.RestartDateAnnotationKey] = time.Now().Format(time.RFC3339)
		if err := c.Patch(ctx, newDep, client.StrategicMergeFrom(&dep, &client.MergeFromWithOptimisticLock{})); err != nil {
			klog.Errorf("Failed to restart Pod, <%s>", newDep.Name)
			return err
		}
		klog.Infof("Restart authenticator server, <%s>", newDep.Name)
	}
	return nil
}
