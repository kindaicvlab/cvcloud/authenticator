/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package start

import (
	"bytes"
	"gotest.tools/assert"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestHttpServer(t *testing.T) {

	start := &startOptions{}
	engine, err := start.run("testdata")
	assert.NilError(t, err)

	var (
		w   = httptest.NewRecorder()
		req *http.Request
	)

	req, err = http.NewRequest("GET", "/kubeconfig/user", nil)
	assert.NilError(t, err)
	engine.ServeHTTP(w, req)
	log.Println(w.Code)
	assert.Equal(t, 200, w.Code)
	assert.Equal(t, `apiVersion: v1
kind: Config`, w.Body.String())

	req, err = http.NewRequest("GET", "/healthz", nil)
	assert.NilError(t, err)
	engine.ServeHTTP(w, req)
	log.Println(w.Code)
	assert.Equal(t, 200, w.Code)

	req, err = http.NewRequest("GET", "/readyz", nil)
	assert.NilError(t, err)
	engine.ServeHTTP(w, req)
	log.Println(w.Code)
	assert.Equal(t, 200, w.Code)

}

const helpDescription = `start http server

Usage:
  start [flags]

Flags:
  -h, --help   help for start
`

func TestStartCommand(t *testing.T) {

	var (
		buf      = &bytes.Buffer{}
		oldStdin = os.Stdin
		start    = NewStartCmd()
	)

	start.SetOut(buf)
	start.SetErr(buf)
	start.SetArgs([]string{"--help"})
	err := start.Execute()
	actual := buf.String()
	os.Stdin = oldStdin
	println(actual)
	assert.NilError(t, err)
	assert.Equal(t, actual, helpDescription)
}
