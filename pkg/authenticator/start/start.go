/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package start

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"

	"gitlab.com/kindaicvlab/cvcloud/authenticator/pkg/authenticator/consts"
)

type startOptions struct{}

// NewStartCmd is command to start authenticator server
func NewStartCmd() *cobra.Command {

	o := startOptions{}
	command := &cobra.Command{
		Use:          "start",
		Short:        "start http server",
		Long:         `start http server`,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			engine, err := o.run(consts.ConfigDir)
			if err != nil {
				return err
			}
			if err = engine.Run(consts.KubeConfigSenderPort); err != nil {
				return err
			}
			return nil
		},
	}

	return command
}

func (o *startOptions) run(kubeconfigDir string) (*gin.Engine, error) {
	engine := gin.Default()
	engine.Static("/kubeconfig", kubeconfigDir)
	engine.GET("/healthz", func(ctx *gin.Context) {
		ctx.JSON(
			http.StatusOK,
			gin.H{
				"code":    http.StatusOK,
				"message": "authenticator is healthy",
			})
	})
	engine.GET("/readyz", func(ctx *gin.Context) {
		ctx.JSON(
			http.StatusOK,
			gin.H{
				"code":    http.StatusOK,
				"message": "authenticator is ready",
			})
	})

	return engine, nil
}
