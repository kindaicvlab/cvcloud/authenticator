/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package create

import (
	"context"
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v3"
	"k8s.io/klog/v2"
	"sigs.k8s.io/controller-runtime/pkg/client"
	kubeyaml "sigs.k8s.io/yaml"

	"gitlab.com/kindaicvlab/cvcloud/authenticator/pkg/authenticator/consts"
	"gitlab.com/kindaicvlab/cvcloud/authenticator/pkg/kubectl"
	"gitlab.com/kindaicvlab/cvcloud/authenticator/pkg/utils"
)

type KubeClient interface {
	GetClient() (client.Client, error)
}

type createOptions struct {
	kubeconfigDir       string
	serviceAccountLabel string
	clusterName         string
	apiServerEndpoint   string
}

// NewCreateCmd is command to generate kubeconfig file
func NewCreateCmd() *cobra.Command {

	o := &createOptions{
		kubeconfigDir: consts.ConfigDir,
	}

	command := &cobra.Command{
		Use:          "create",
		Short:        "",
		Long:         `create kubeconfig for ServiceAccount`,
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			cli := &kubectl.KubeClient{}
			c, err := cli.GetClient()
			if err != nil {
				return err
			}
			return o.run(context.Background(), c)
		},
	}
	f := command.Flags()
	f.StringVarP(&o.serviceAccountLabel, "selector", "l", "", "selector for ServiceAccount (eg. project=alpha)")
	f.StringVarP(&o.clusterName, "cluster-name", "c", "kubernetes", "cluster name for kubeconfig")
	f.StringVarP(&o.apiServerEndpoint, "api-server", "e", "127.0.0.1:6443", "endpoint for kube-apiserver")

	return command
}

func (o *createOptions) run(ctx context.Context, c client.Client) error {

	splitSelector, err := utils.ValidateSelector(o.serviceAccountLabel)
	if err != nil {
		klog.Errorf("Failed to verify selector, <%s>.", o.serviceAccountLabel)
		return err
	}
	serviceAccounts, err := kubectl.GetServiceAccount(ctx, c, splitSelector)
	if err != nil {
		klog.Errorf("Failed to get ServiceAccount.")
		return err
	}
	if len(serviceAccounts.Items) == 0 {
		klog.Errorf("There are not any ServiceAccounts in cluster.")
		return fmt.Errorf("failed to find ServiceAccounts")
	}

	if !fileExists(o.kubeconfigDir) {
		if err = os.MkdirAll(o.kubeconfigDir, 0660); err != nil {
			return err
		}
	}

	for _, serviceAccount := range serviceAccounts.Items {

		secretName := serviceAccount.Secrets[0].Name
		namespace := serviceAccount.Namespace

		secret, err := kubectl.GetSecret(ctx, c, secretName, namespace)
		if err != nil {
			klog.Errorf("Failed to find Secret.")
			return err
		}
		token := string(secret.Data["token"])

		ca := secret.Data["ca.crt"]
		caData := base64.StdEncoding.EncodeToString(ca)

		// Create kubeconfig
		if err = o.createKubeConfig(namespace, serviceAccount.Name, token, caData); err != nil {
			klog.Errorf("Failed to generate kubeconfig.")
			return err
		}
		klog.Infof("Generated kubeconfig for ServiceAccount, <%s>", serviceAccount.Name)
	}

	return nil
}

func (o *createOptions) createKubeConfig(namespace, serviceAccount, token, caData string) error {
	kubeConfig := KubeConfig{
		APIVersion: "v1",
		Clusters: []Clusters{
			{
				Cluster: Cluster{
					Ca:     caData,
					Server: o.apiServerEndpoint,
				},
				ClusterName: o.clusterName,
			},
		},
		Contexts: []Contexts{
			{
				Context: Context{
					ContextsCluster:  o.clusterName,
					ContextNamespace: namespace,
					ContextUser:      serviceAccount,
				},
				ContextName: o.clusterName,
			},
		},
		CurrentContext: o.clusterName,
		Kind:           "Config",
		Users: []Users{
			{
				UserName: serviceAccount,
				User: User{
					UserToken: token,
				},
			},
		},
	}

	buf, err := yaml.Marshal(kubeConfig)
	if err != nil {
		return err
	}
	var unstructured interface{}
	if err = kubeyaml.Unmarshal(buf, &unstructured); err != nil {
		return err
	}
	encoded, err := kubeyaml.Marshal(&unstructured)
	if err != nil {
		return err
	}

	kubeConfigFilePath := filepath.Join(o.kubeconfigDir, serviceAccount)
	if err = os.WriteFile(kubeConfigFilePath, encoded, 0600); err != nil {
		return err
	}

	return nil
}

func fileExists(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil
}
