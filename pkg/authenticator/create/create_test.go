/*
Copyright © 2022 KindaiCVLAB

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package create

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
)

var (
	// {Selector: ["ServiceAccount Name"...]}
	testServiceAccounts = map[string][]string{
		"project=test-a": {"product-0-developer", "product-1-developer", "product-2-developer"},
		"project=test-b": {"product-3-developer", "product-4-developer", "product-5-developer"},
	}
)

func TestCreateCommand(t *testing.T) {

	testCases := []struct {
		description string
		selectorOpt string
		// {Name: ObjKind}
		deleteObj map[string]string
		wantErr   bool
	}{
		{
			description: "Positive Case with selector for ServiceAccount",
			selectorOpt: "project=test-a",
			wantErr:     false,
		},
		{
			description: "Positive Case without selector for ServiceAccount",
			wantErr:     false,
		},
		{
			description: "Failed to get ServiceAccount",
			wantErr:     true,
			selectorOpt: "project=test-b",
			deleteObj: func() map[string]string {
				objects := make(map[string]string)
				for _, name := range testServiceAccounts["project=test-b"] {
					objects[name] = "ServiceAccount"
				}
				return objects
			}(),
		},
		{
			description: "Failed to get Secret",
			wantErr:     true,
			selectorOpt: "project=test-a",
			deleteObj: func() map[string]string {
				objects := make(map[string]string)
				for _, name := range testServiceAccounts["project=test-a"] {
					objects[name] = "Secret"
				}
				return objects
			}(),
		},
		{
			description: "Invalid Selector Option",
			wantErr:     true,
			selectorOpt: "test-a",
		},
	}

	for _, test := range testCases {
		t.Run(test.description, func(t *testing.T) {
			tmpDir, err := os.MkdirTemp("", "authenticator_create_test_dir")
			if err != nil {
				log.Fatal(err)
			}

			o := &createOptions{
				kubeconfigDir:       tmpDir,
				serviceAccountLabel: test.selectorOpt,
			}
			cli := &kubeClientMock{}
			c, err := cli.GetClient()
			if err != nil {
				t.Fatal(err)
			}
			ctx := context.Background()

			if test.deleteObj != nil {
				if err = deleteTestObj(ctx, test.deleteObj, c); err != nil {
					t.Fatal(err)
				}
			}

			err = o.run(ctx, c)
			if (err != nil) != test.wantErr {
				t.Errorf("expected error: %v, got: %v\n", test.wantErr, err)
			} else if !test.wantErr {
				if test.selectorOpt == "" {
					for _, users := range testServiceAccounts {
						for _, u := range users {
							if _, exist := test.deleteObj[u]; exist || fileExists(filepath.Join(o.kubeconfigDir, u)) {
								continue
							}
							t.Errorf("Failed to find kubeconfig for serviceacount, %s", u)
						}
					}
				} else {
					for _, u := range testServiceAccounts[o.serviceAccountLabel] {
						if _, exist := test.deleteObj[u]; exist || fileExists(filepath.Join(o.kubeconfigDir, u)) {
							continue
						}
						t.Errorf("Failed to find kubeconfig for serviceacount, %s", u)
					}
				}
			}
			if err := os.RemoveAll(tmpDir); err != nil {
				t.Fatal(err)
			}
		})
	}
}

func deleteTestObj(ctx context.Context, deleteObj map[string]string, c client.Client) error {
	for name, objKind := range deleteObj {
		var obj client.Object
		switch objKind {
		case "Secret":
			obj = &corev1.Secret{}
		case "ServiceAccount":
			obj = &corev1.ServiceAccount{}
		default:
			return fmt.Errorf("you must specify 'ServiceAccount' or 'Secret'")
		}

		if err := c.Get(ctx, client.ObjectKey{Name: name, Namespace: name}, obj); err != nil {
			return err
		}
		if err := c.Delete(ctx, obj, &client.DeleteOptions{}); err != nil {
			return err
		}
	}
	return nil
}

type kubeClientMock struct{}

func (c *kubeClientMock) GetClient() (client.Client, error) {
	scm := runtime.NewScheme()
	if err := scheme.AddToScheme(scm); err != nil {
		return nil, err
	}
	fakeClientBuilder := fake.NewClientBuilder().WithScheme(scm)

	for selector, accounts := range testServiceAccounts {
		// create selector
		splitSelector := strings.Split(selector, "=")
		inputSelector := map[string]string{splitSelector[0]: splitSelector[1]}

		for _, name := range accounts {
			serviceAccount := newFakeServiceAccount(name, inputSelector)
			secret := newFakeSecret(name)
			fakeClientBuilder.
				WithObjects(serviceAccount).
				WithObjects(secret)
		}
	}
	return fakeClientBuilder.Build(), nil
}

func newFakeServiceAccount(name string, selector map[string]string) *corev1.ServiceAccount {
	return &corev1.ServiceAccount{
		TypeMeta: metav1.TypeMeta{
			APIVersion: corev1.SchemeGroupVersion.String(),
			Kind:       "ServiceAccount",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: name,
			Labels:    selector,
		},
		Secrets: []corev1.ObjectReference{
			{
				APIVersion: corev1.SchemeGroupVersion.String(),
				Kind:       "Secret",
				Name:       name,
				Namespace:  name,
			},
		},
	}
}

func newFakeSecret(name string) *corev1.Secret {
	return &corev1.Secret{
		TypeMeta: metav1.TypeMeta{
			APIVersion: corev1.SchemeGroupVersion.String(),
			Kind:       "Secret",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: name,
		},
		Data: map[string][]byte{
			"token": []byte("foobar"),
			"ca.crt": []byte(`-----BEGIN CERTIFICATE-----
MIIC5zCCAc+gAwIBAgIBADANBgkqhkiG9w0BAQsFADAVMRMwEQYDVQQDEwprdWJl
cm5ldGVzMB4XDTIyMDIwNjE1NDE0M1oXDTMyMDIwNDE1NDE0M1owFTETMBEGA1UE
AxMKa3ViZXJuZXRlczCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJvF
959kXTCYuNc6LgNysswLHKRwaU4h2uD9GHXYJ3RCRfstJ12A9MJ6zYD0nnW0lnJd
r5kKL8Po205kP2iAobrdE59Rv/t2wyMjOq0RZ0gpyyrCCJC9qz7IJAFl8YW1sTSr
iLblgcOUBGmrTXB2Nym+jxb3dQ7esKAwFcoKPCiOuiz+5I7ctcSBcr91TGjqAS4w
+nKtN0trM+JD+1Xx6fjlKVniIc+NIXEXTPvxroWRWioVsHrf7rDPH+qHDJvYeO89
0bpa5RxvdrGCeE6Fu0GjGGBmT+3m8jksIxEKRrlnsrfbvN4u9Y4YZ2IbjAMvKGwl
Q596E3ZbMPabtKhEZU0CAwEAAaNCMEAwDgYDVR0PAQH/BAQDAgKkMA8GA1UdEwEB
/wQFMAMBAf8wHQYDVR0OBBYEFPHrcoPsSbgiuKUIPVWUxCrHtwaJMA0GCSqGSIb3
DQEBCwUAA4IBAQAKMSJyEOjkIBx8zSnxCsGyostdWK125Cy4y3jt8FJvSKc6wlBY
i+eafhRLzbiyMBbyk3gyR3u6lmJJJiVgto5Li1DjWscQZRJqayGqJs/pzpxkoV6/
pjf7nsHooAsy8nQfo1lfkF/qt9PoMEpbjNl151MXedhPBsgW0pw953fv0VZT/Xbu
NZLxUFzoPRFUqszakphQIxIZ9/tXPNqNQblCkaV20Rm3j8lrU2u1IZYjpaEfjYQ5
7hVQeaeOuDEyZSK+Qi8IDtWcks7PS32KYN4lZrua3UwSuqQDUjT4RHfxlZjuK7Ss
2/fUi+3d0KuzBRFkIPpDo0DKfHEkM31TG8hW
-----END CERTIFICATE-----
`),
		},
	}
}
