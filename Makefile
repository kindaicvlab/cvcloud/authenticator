AUTHENTICATOR_IMG ?= registry.gitlab.com/kindaicvlab/cvcloud/authenticator/authenticator:latest
CONFIG_RELOADER_IMG ?= registry.gitlab.com/kindaicvlab/cvcloud/authenticator/config-reloader:latest

.PHONY: check
check: mod fmt vet golangci-lint bundle-manifests

.PHONY: bundle-manifests
bundle-manifests:
	kustomize build manifests/overlays/release > deploy/manifests.yaml

.PHONY: build-test
build-test:
	go build -o bin/test_authenticator cmd/authenticator/main.go
	go build -o bin/test_config-reloader cmd/config-reloader/main.go

.PHONY: docker-build
docker-build:
	docker build -t "${AUTHENTICATOR_IMG}" -f cmd/authenticator/Dockerfile .
	docker build -t "${CONFIG_RELOADER_IMG}" -f cmd/config-reloader/Dockerfile .

.PHONY: test
test:
	go test -count=1 -coverprofile cover.out ./... && go tool cover -func cover.out

.PHONY: fmt
fmt: ## Run go fmt against code.
	go fmt ./...

.PHONY: vet
vet: ## Run go vet against code.
	go vet ./...

.PHONY: mod
mod:
	go mod tidy

.PHPNY: golangci-lint
golangci-lint:
ifeq ("$(shell golangci-lint version 2>/dev/null)", "")
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.44.0
endif
	golangci-lint run ./... --timeout 3m

RELEASE_VERSION ?= v0.0.0
.PHONY: prepare-release
prepare-release: check test
	RELEASE_VERSION="$(RELEASE_VERSION)" yq eval -i '.commonLabels."app.kubernetes.io/version"|=env(RELEASE_VERSION)' ./manifests/components/authenticator/kustomization.yaml
	RELEASE_VERSION="$(RELEASE_VERSION)" yq eval -i '.images[0].newTag|=env(RELEASE_VERSION)' ./manifests/components/authenticator/applications/kustomization.yaml
	RELEASE_VERSION="$(RELEASE_VERSION)" yq eval -i '.commonLabels."app.kubernetes.io/version"|=env(RELEASE_VERSION)' ./manifests/components/config-reloader/kustomization.yaml
	RELEASE_VERSION="$(RELEASE_VERSION)" yq eval -i '.images[0].newTag|=env(RELEASE_VERSION)' ./manifests/components/config-reloader/applications/kustomization.yaml
	$(MAKE) bundle-manifests

.PHONY: kind-start
kind-start:
	bash ./hack/kind-start.sh

.PHONY: load-image
load-image: docker-build
	kind load docker-image "${AUTHENTICATOR_IMG}"
	kind load docker-image "${CONFIG_RELOADER_IMG}"

.PHONY: integration-test
integration-test: load-image
	bash ./hack/integration-test.sh
