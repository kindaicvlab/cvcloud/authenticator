#!/usr/bin/env bash
# Copyright © 2022 KindaiCVLAB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

cd "$(dirname "$0")"
set -eox pipefail

TIMEOUT=5m
AUTHENTICATOR_NS=authenticator
TEST_NS=test-product
TEST_SERVICEACCOUNT=test-product-developer

function prepare() {
  # Prepare test namespace
  kustomize build ../integration-test/rbac | kubectl apply -f -

  # Set api-server for authenticator
  API_SERVER="$(yq eval '.clusters[] | select(.name=="kind-kind").cluster.server' ~/.kube/config)"
  ARG="--api-server=${API_SERVER}" yq eval -i '.[0].value[3]|=env(ARG)' \
    ../manifests/overlays/integration-test/patches/authenticator_deploy.yaml
}

function setup_authenticator() {
  # Deploy authenticator
  kustomize build ../manifests/overlays/integration-test | kubectl apply -f -
  kubectl wait pods -n "${AUTHENTICATOR_NS}" --for condition=ready --timeout="${TIMEOUT}" -l app.kubernetes.io/name=authenticator
  sleep 5
  kubectl get pods -n "${AUTHENTICATOR_NS}"
}

function _get_kubeconfig() {
  kubectl port-forward -n "${AUTHENTICATOR_NS}" svc/authenticator 10500:80 & \
    curl "127.0.0.1:10500/kubeconfig/${TEST_SERVICEACCOUNT}" > ../testconfig
    # Tear Down port-forward
    kill %1 || true
}

function start_test() {
  # Test for authenticator
  _get_kubeconfig
  kubectl get pods -n "${TEST_NS}" --kubeconfig ../testconfig

  kubectl create job test-config-reloader -n "${AUTHENTICATOR_NS}" --from cronjob/config-reloader
  kubectl wait job test-config-reloader -n "${AUTHENTICATOR_NS}" --for=condition=complete --timeout="${TIMEOUT}"
  kubectl get pods -n "${AUTHENTICATOR_NS}"
}

function teardown() {
  kubectl delete job -n "${AUTHENTICATOR_NS}" test-config-reloader
  kustomize build ../integration-test/rbac | kubectl delete -f -
  kustomize build ../manifests/overlays/integration-test | kubectl delete -f - || true
  git checkout origin/head -- ../manifests/overlays/integration-test/patches/authenticator_deploy.yaml
}

function main() {
    prepare || (teardown && exit 1)
    setup_authenticator || (teardown && exit 1)
    start_test || (teardown && exit 1)
    teardown
}

main
