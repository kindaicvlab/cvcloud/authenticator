#!/usr/bin/env bash
# stolen from https://github.com/tenzen-y/imperator/blob/0139f28bc494e7355975a683924da4efa166ed60/hack/kind-start.sh and modified.
# Copyright 2021 Yuki Iwai (@tenzen-y)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -eo pipefail
cd "$(dirname "$0")"

kind create cluster --config ../integration-test/kind-config.yaml
kubectl config use-context kind-kind

# Wait for Start Kind Node
TIMEOUT=5m
kubectl wait --for condition=ready --timeout="${TIMEOUT}" node kind-control-plane
